<?php

declare(strict_types=1);

/*
Copyright (C) 2021, George Zeakis <zeageorge@gmail.com> FORTH-ICS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace zeageorge\khmdhs_agent;

use Throwable;
use Exception;
use RuntimeException;
use GuzzleHttp\{
  Client, 
  Psr7\Response,
  Exception\GuzzleException
};

use function 
  unlink,
  tempnam,
  basename,
  mb_strpos,
  is_readable,
  json_encode,
  json_decode,
  json_last_error,
  array_key_exists,
  sys_get_temp_dir,
  file_get_contents;

use const 
  JSON_ERROR_NONE,
  JSON_UNESCAPED_UNICODE,
  JSON_UNESCAPED_SLASHES;

/**
 * Description of KhmdhsClient
 *
 * @author George Zeakis <zeageorge@gmail.com>
 * @license GNU Affero General Public License version 3
 */
class KhmdhsClient {
  protected $request_types = [
    'prwtogenes_aithma' => 'requests',
    'egkekrimeno_aithma' => 'requests',
    'prokhry3h_diakhry3h_apo_egkekrimeno_aithma' => 'notices',
    'apofash_katakyrwshs_ana8eshs_apo_egkekrimeno_aithma' => 'auctions',
    'apofash_katakyrwshs_ana8eshs_apo_prokhry3h_diakhry3h' => 'auctions',
    'symbash_apo_apofash_katakyrwshs_ana8eshs' => 'contracts',
    'entolh_plhrwmhs_apo_symbash' => 'payments',
    'entolh_plhrwmhs_apo_apofash_katakyrwshs_ana8eshs' => 'payments',
  ];

  /** @var string The API base URL */
  protected $khmdhs_service_base_url;

  /** 
   * see error: cURL error 60: SSL certificate problem: unable to get local issuer certificate:
   * https://curl.se/docs/sslcerts.html
   * https://curl.se/docs/caextract.html
   * https://docs.guzzlephp.org/en/stable/request-options.html#verify-option
   * https://stackoverflow.com/questions/29822686/curl-error-60-ssl-certificate-unable-to-get-local-issuer-certificate
   * https://stackoverflow.com/questions/24611640/curl-60-ssl-certificate-problem-unable-to-get-local-issuer-certificate
   * https://stackoverflow.com/questions/24923604/guzzle-curl-error-60-ssl-unable-to-get-local-issuer
   * 
   * @var string The full path to the .pem file for ssl certificate verification.
   */
  protected $cert_path;

  /**
   * 
   * @param string $khmdhs_service_base_url default is https://khmdhs-service.ics.forth.gr/api/v1_1/
   * @param string $cert_path The full path to the .pem file for ssl certificate verification. default is khmdhs-service_ics_forth_gr.pem
   */
  public function __construct(
    string $khmdhs_service_base_url = 'https://khmdhs-service.ics.forth.gr/api/v1_1/',
    string $cert_path = __DIR__ . '/../khmdhs-service_ics_forth_gr1.pem'
  ) {
    $this->khmdhs_service_base_url = $khmdhs_service_base_url;
    $this->cert_path = $cert_path;
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               this object must also contain a property named 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publish($request): string {
    if (!array_key_exists($request->request_type ?? '-1', $this->request_types)) {
      throw new Exception('Unknown request type');
    }

    /** @var string */
    $filepath = $request->filepath;

    unset($request->filepath);

    /** @var string|false */
    $data = json_encode($request, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
  
    $http_client = new Client(['verify' => $this->cert_path, 'base_uri' => $this->khmdhs_service_base_url, 'timeout' => 0, 'connect_timeout' => 60 * 5]);

    /** @var Response */
    $http_response = $http_client->request('POST', $this->request_types[$request->request_type], [
      'http_errors' => false, // http://docs.guzzlephp.org/en/stable/request-options.html#http-errors
      'multipart' => [
        ['name' => 'data', 'contents' => $data],
        ['name' => 'file', 'contents' => file_get_contents($filepath), 'filename' => basename($filepath)]
      ]
    ]);

    /** @var string */
    $http_response_body = $http_response->getBody()->getContents();

    if ($http_response->getStatusCode() != 200 || mb_strpos($http_response_body, '"adam":"') === false) {
      $e = new Exception();

      $e->http_response_status_code = $http_response->getStatusCode();
      $e->http_response_body = $http_response_body;

      throw $e;
    }

    $decoded_body = json_decode($http_response_body);

    /** @var int */
    $json_error = json_last_error();

    if ($json_error !== JSON_ERROR_NONE || !(isset($decoded_body->data) && isset($decoded_body->data->adam))) {
      $e = new Exception();

      $e->http_response_status_code = $http_response->getStatusCode();
      $e->http_response_body = $http_response_body;

      throw $e;
    }

    return $decoded_body->data->adam;
  }

  /**
   * 
   * @param string $publication_type
   * @param string $adam
   * 
   * @return string the pdf file contents
   * @throws Exception
   * @throws GuzzleException
   */
  public function getPublishedFile(string $publication_type, string $adam): string {
    if (!array_key_exists($publication_type, $this->request_types)) {
      throw new Exception('Unknown publication type');
    }

    if (empty($adam)) { 
      throw new Exception('ADAM validation failed');
    }

    try {
      if (false === ($tmpname = tempnam(sys_get_temp_dir(), 'khmdhs_agent_'))) {
        throw new Exception("Can't create a temp file");
      }

      /** @var Client */
      $http_client = new Client(['verify' => $this->cert_path, 'base_uri' => $this->khmdhs_service_base_url, 'timeout' => 0, 'connect_timeout' => 60 * 5]);

      /** @var Response */
      $http_response = $http_client->request("GET", $this->request_types[$publication_type] . "/{$adam}/files/0", [
        'http_errors' => false, // http://docs.guzzlephp.org/en/stable/request-options.html#http-errors
        'sink' => $tmpname
      ]);

      $pdf_file_contents = file_get_contents($tmpname);

      unlink($tmpname);

      return $pdf_file_contents;
    } catch (Throwable $t) {
      isset($tmpname) && is_readable($tmpname) && unlink($tmpname);

      throw $t;
    }
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               and https://khmdhs-service.ics.forth.gr/schemas/prwtogenes_aithma.schema.json
   *               this object must also contain a property 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publishPrwtogenesAithma($request): string {
    return $this->publish($request);
  }

  /**
   * 
   * @param string $adam
   * 
   * @return string the pdf file contents
   * @throws Exception
   * @throws GuzzleException
   */
  public function getPrwtogenesAithmaPublishedFile(string $adam): string {
    return $this->getPublishedFile('prwtogenes_aithma', $adam);
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               and https://khmdhs-service.ics.forth.gr/schemas/egkekrimeno_aithma.schema.json
   *               this object must also contain a property 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publishEgkekrimenoAithma($request): string {
    return $this->publish($request);
  }

  /**
   * 
   * @param string $adam
   * 
   * @return string the pdf file contents
   * @throws Exception
   * @throws GuzzleException
   */
  public function getEgkekrimenoAithmaPublishedFile(string $adam): string {
    return $this->getPublishedFile('egkekrimeno_aithma', $adam);
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               and https://khmdhs-service.ics.forth.gr/schemas/prokhry3h_diakhry3h_apo_egkekrimeno_aithma.schema.json
   *               this object must also contain a property 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publishSuppliersInvitation($request): string {
    return $this->publish($request);
  }

  /**
   * 
   * @param string $adam
   * 
   * @return string the pdf file contents
   * @throws Exception
   * @throws GuzzleException
   */
  public function getSuppliersInvitationPublishedFile(string $adam): string {
    return $this->getPublishedFile('prokhry3h_diakhry3h_apo_egkekrimeno_aithma', $adam);
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               and https://khmdhs-service.ics.forth.gr/schemas/apofash_katakyrwshs_ana8eshs_apo_egkekrimeno_aithma.schema.json
   *               this object must also contain a property 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publishApofashKatakyrwshsAna8eshsApoEgkekrimenoAithma($request): string {
    return $this->publish($request);
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               and https://khmdhs-service.ics.forth.gr/schemas/apofash_katakyrwshs_ana8eshs_apo_prokhry3h_diakhry3h.schema.json
   *               this object must also contain a property 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publishApofashKatakyrwshsAna8eshsApoProkhry3hDiakhry3h($request): string {
    return $this->publish($request);
  }

  /**
   * 
   * @param string $adam
   * 
   * @return string the pdf file contents
   * @throws Exception
   * @throws GuzzleException
   */
  public function getApofashKatakyrwshsAna8eshsPublishedFile(string $adam): string {
    return $this->getPublishedFile('apofash_katakyrwshs_ana8eshs_apo_egkekrimeno_aithma', $adam);
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               and https://khmdhs-service.ics.forth.gr/schemas/symbash_apo_apofash_katakyrwshs_ana8eshs.schema.json
   *               this object must also contain a property 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publishSymbashApoApofashKatakyrwshsAna8eshs($request): string {
    return $this->publish($request);
  }

  /**
   * 
   * @param string $adam
   * 
   * @return string the pdf file contents
   * @throws Exception
   * @throws GuzzleException
   */
  public function getSymbashApoApofashKatakyrwshsAna8eshsPublishedFile(string $adam): string {
    return $this->getPublishedFile('symbash_apo_apofash_katakyrwshs_ana8eshs', $adam);
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               and https://khmdhs-service.ics.forth.gr/schemas/entolh_plhrwmhs_apo_symbash.schema.json
   *               this object must also contain a property 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publishApofashEgkrishsDapanhsApoSymbash($request): string {
    return $this->publish($request);
  }

  /**
   * 
   * @param string $adam
   * 
   * @return string the pdf file contents
   * @throws Exception
   * @throws GuzzleException
   */
  public function getApofashEgkrishsDapanhsApoSymbashPublishedFile(string $adam): string {
    return $this->getPublishedFile('entolh_plhrwmhs_apo_symbash', $adam);
  }

  /**
   * 
   * @param object $request see https://khmdhs-service.ics.forth.gr/schemas/request.schema.json 
   *               and https://khmdhs-service.ics.forth.gr/schemas/entolh_plhrwmhs_apo_apofash_katakyrwshs_ana8eshs.schema.json
   *               this object must also contain a property 'filepath' for the pdf file to publish
   * 
   * @return string The ADAM
   * @throws Exception
   * @throws GuzzleException
   * @throws RuntimeException
   */
  public function publishApofashEgkrishsDapanhsApoApofashKatakyrwshsAna8eshs($request): string {
    return $this->publish($request);
  }

  /**
   * 
   * @param string $adam
   * 
   * @return string the pdf file contents
   * @throws Exception
   * @throws GuzzleException
   */
  public function getApofashEgkrishsDapanhsApoApofashKatakyrwshsAna8eshsPublishedFile(string $adam): string {
    return $this->getPublishedFile('entolh_plhrwmhs_apo_apofash_katakyrwshs_ana8eshs', $adam);
  }

  /**
   * 
   * @return string
   */
  public function getKhmdhsServiceBaseUrl(): string {
    return $this->khmdhs_service_base_url;
  }

  /**
   * 
   * @param string $khmdhs_service_base_url
   * @return self
   */
  public function setKhmdhsServiceBaseUrl(string $khmdhs_service_base_url): self {
    $this->khmdhs_service_base_url = $khmdhs_service_base_url;

    return $this;
  }

  /**
   * 
   * @return string
   */
  public function getCertPath(): string {
    return $this->cert_path;
  }

  /**
   * 
   * @param string $cert_path
   * @return self
   */
  public function setCertPath(string $cert_path): self {
    $this->cert_path = $cert_path;

    return $this;
  }

}

