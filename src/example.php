<?php

declare(strict_types=1);

/*
Copyright (C) 2021, George Zeakis <zeageorge@gmail.com> FORTH-ICS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace zeageorge\khmdhs_agent;

require '../vendor/autoload.php';

use Throwable;
use zeageorge\khmdhs_agent\KhmdhsClient;
use function 
  date,
  file_put_contents;

try {
  $cert_path = __DIR__ . '/../khmdhs-service_ics_forth_gr.pem';

  $khmdhs_client = (new KhmdhsClient())->setCertPath($cert_path);

  $request = (object) [
    'khmdhs_account' => (object) [
      'username' => 'your username',
      'password' => 'your password',
    ],
    'request_type' => 'prwtogenes_aithma',
    'request_data' => (object) [
      'basic_document_data' => (object) [
        'title' => 'Test',
        'signed_date' => date("d/m/Y"),
        'protocol_number' => '-1',
        'author_email_address' => 'zeageorge@ics.forth.gr',
        'units_operator' => 'units_operator_id', 
        'signer' => 'signer_id', 
        'request_check_vat' => false,
        'vat_number' => '090101655',
        'nuts_code' => 'EL431',
        'contract_types' => ['13'],
      ],
      'approved_notices' => [(object) [
        'quantity' => 1,
        'measurement_unit' => "C62",
        'total_cost' => 1,
        'vat' => "24",
        'currency' => "EUR",
        'description' => 'Test item',
        'cpvs' => ["14212200-2"]
      ]]
    ],
    'filepath' => '../test.pdf',
  ];

  /** @var string */
  $adam = $khmdhs_client->publishPrwtogenesAithma($request);

  echo "ADAM: {$adam}\n";

  /** @var string */
  $published_pdf_file_contents = $khmdhs_client->getPrwtogenesAithmaPublishedFile($adam);

  file_put_contents(__DIR__ . "/{$adam}.pdf", $published_pdf_file_contents);
} catch (Throwable $t) {
  echo $t->getMessage();

  if (isset($t->http_response_status_code)) {
    echo "http_response_status_code: {$t->http_response_status_code}\n";
  }

  if (isset($t->http_response_body)) {
    echo "http_response_body: {$t->http_response_body}\n";
  }

  throw $t;
}
