# KHMDHS Agent #

A simple client to use for Khmdhs API Service


## Links

[KHMDHS Manual](http://www.eprocurement.gov.gr/webcenter/files/KHMDHS/NewManualKHMDHS_8_2018.pdf)

[KHMDHS FAQ](http://www.eprocurement.gov.gr/webcenter/files/KHMDHS/SYXNES_EROTHSEIS_KHMDHS.pdf)

[KHMDHS Search](http://www.eprocurement.gov.gr/kimds2/unprotected/searchRequests.htm)



## JSON Schemas for each publication request

each request object must also contain a property named 'filepath' for the pdf file to publish

[request](https://khmdhs-service.ics.forth.gr/schemas/request.schema.json)

[Νέο Πρωτογενές Αίτημα](https://khmdhs-service.ics.forth.gr/schemas/prwtogenes_aithma.schema.json)

[Εγκεκριμένο αίτημα](https://khmdhs-service.ics.forth.gr/schemas/egkekrimeno_aithma.schema.json)

[Νέα Προκήρυξη-Διακήρυξη](https://khmdhs-service.ics.forth.gr/schemas/prokhry3h_diakhry3h_apo_egkekrimeno_aithma.schema.json)

[Νέα Απόφαση Κατακύρωσης - Ανάθεσης από Εγκ. Αιτήματα](https://khmdhs-service.ics.forth.gr/schemas/apofash_katakyrwshs_ana8eshs_apo_egkekrimeno_aithma.schema.json)

[Νέα Απόφαση Κατακύρωσης - Ανάθεσης από Προκήρυξη-Διακήρυξη](https://khmdhs-service.ics.forth.gr/schemas/apofash_katakyrwshs_ana8eshs_apo_prokhry3h_diakhry3h.schema.json)

[Νέα Σύμβαση από Απόφαση Κατακύρωσης - Ανάθεσης](https://khmdhs-service.ics.forth.gr/schemas/entolh_plhrwmhs_apo_symbash.schema.json)

[Νέα Εντολή Πληρωμής από Σύμβαση](https://khmdhs-service.ics.forth.gr/schemas/entolh_plhrwmhs_apo_symbash.schema.json)

[Νέα Εντολή Πληρωμής από Απόφαση Κατακύρωσης - Ανάθεσης](https://khmdhs-service.ics.forth.gr/schemas/entolh_plhrwmhs_apo_apofash_katakyrwshs_ana8eshs.schema.json)



## Examples

see [example.php](src/example.php)


## License
Copyright (C) 2021, George Zeakis <zeageorge@gmail.com> FORTH-ICS

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License version 3  
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License  
along with this program.  If not, see <https://www.gnu.org/licenses/>.
